from django.conf.urls import *
from django.contrib import admin
from agenda import urls as agendaUrl

urlpatterns = [
    url(r'^', include(agendaUrl)),
    url(r'^admin/', include(admin.site.urls)),
]

