#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-
import logging
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.views import View
from .forms import ContactForm
from .models import *

logger = logging.getLogger(__name__)


class ContactDelete(View):
    def get(self, request):
        try:
            idContact   = request.GET['id']
            Contact.objects.get(pk=idContact).delete()
            return redirect('index')
        except Exception as e:
            logger.exception(e)
            return redirect('index')


class ContactEdit(View):
    def post(self, request, *args, **kwargs):
        form = ContactForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            contactData     = {}
            phoneContact    = {}
            emailContact    = {}

            for key in data:
                if data[key] is not None:
                    if key in ['id', 'firstName', 'lastName', 'notes', 'birthday', 'address']:
                        if data[key] is not None:
                            contactData[key] = data[key]
                    if key in ['email']:
                        if data[key] is not None:
                            emailContact[key] = data[key]
                    if key in ['phoneNumber']:
                        if data[key] is not None:
                            phoneContact[key] = data[key]

            if len(contactData) > 0:
                if data['id'] is not None:
                    contact = Contact(pk=data['id'], **contactData)
                else:
                    contact = Contact(**contactData)
                contact.save()

                if contact.id:
                    email = Email.objects.filter(contact_id=contact)
                    phone = Telephone.objects.filter(contact_id=contact)
                    if len(email):
                        email = updateObject(email.get(), **emailContact)
                    else:
                        email = Email(**emailContact)
                        email.contact = contact

                    if len(phone):
                        phone = updateObject(phone.get(), **phoneContact)
                    else:
                        phone = Telephone(**phoneContact)
                        phone.contact = contact

                    phone.save()
                    email.save()

        return redirect('index')

    def get(self, request, *args, **kwargs):
        try:
            idContact       = request.GET['id']
            contact         = Contact.objects.get(pk=idContact)
            contactEmail    = Email.objects.filter(contact_id=contact)
            contactPhone    = Telephone.objects.filter(contact_id=contact)

            dataContact = {
                "id"            : contact.id,
                "firstName"     : contact.firstName,
                "lastName"      : contact.lastName,
                "phoneNumber"   : contactPhone.get().phoneNumber if len(contactPhone) else None,
                "email"         : contactEmail.get().email if len(contactEmail) else None,
                "address"       : contact.address,
                "birthday"      : contact.birthday,
                "notes"         : contact.notes
            }

            form = ContactForm(initial=dataContact)
            data = {
                'form'      : form,
                'formUrl'   : request.path,
                'contactId' : contact.id
            }
            return render(request, 'editContact.html', data)
        except Exception as e:
            logger.exception(e)
            return redirect('index')


class ContactCreation(View):

    def get(self, request, *args, **kwargs):
        form = ContactForm()
        data = {
            'form'      : form,
            'formUrl'   : reverse('edit')
        }
        return render(request, 'editContact.html', data)


class ContactList(View):

    def get(self, request, *args, **kwargs):
        contactsQuery = Contact.objects.all()
        contacts = []
        for contact in contactsQuery:
            contactEmail = Email.objects.filter(contact_id=contact)
            contactPhone = Telephone.objects.filter(contact_id=contact)

            contactData = {
                "id"            : contact.id,
                "firstName"     : contact.firstName,
                "lastName"      : contact.lastName,
                "phoneNumber"   : contactPhone.get().phoneNumber if len(contactPhone) else None,
                "email"         : contactEmail.get().email if len(contactEmail) else None,
                "address"       : contact.address,
                "birthday"      : contact.birthday,
                "notes"         : contact.notes
            }

            contacts.append(contactData)

        return render(request, 'index.html', {'Contacs': contacts})
