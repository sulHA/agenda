#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-
import datetime
from django.forms.extras.widgets import SelectDateWidget
from django import forms
from .models import Contact
import logging
logger = logging.getLogger(__name__)

this_year = datetime.date.today().year
prev_years = range(this_year - 100, this_year + 1)
years = sorted(prev_years, reverse=True)
widgetDate = SelectDateWidget(years=years)


class ContactForm(forms.Form):
    class Meta:
        model = Contact
    id                  = forms.IntegerField(required=False, widget=forms.HiddenInput)
    firstName           = forms.CharField(required=True, label='First Name')
    lastName            = forms.CharField(required=False, label='Last Name')
    address              = forms.CharField(required=False)
    birthday            = forms.DateField(required=False, widget=widgetDate)
    notes               = forms.CharField(required=False, widget=forms.Textarea, max_length=500)
    phoneNumber         = forms.CharField(required=False, label='Telephone')
    email               = forms.EmailField(required=False)
