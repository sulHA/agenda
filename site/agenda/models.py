#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
import datetime


class Meta:
    app_label = 'agenda'

def updateObject(obj, **kwargs):
    for k, v in kwargs.items():
        setattr(obj, k, v)
    return obj

class Contact(models.Model):

    firstName   = models.TextField(null=True)
    lastName    = models.TextField(null=True)
    address     = models.TextField(null=True)
    birthday    = models.DateField(null=True)
    notes       = models.TextField(max_length=500)


class Telephone (models.Model):
    contact     = models.ForeignKey(Contact, on_delete=models.CASCADE)
    phoneType   = models.TextField(null=True)
    phoneNumber = models.TextField(null=True)


class Email(models.Model):
    contact     = models.ForeignKey(Contact, on_delete=models.CASCADE)
    email       = models.EmailField(null=True)
    emailType   = models.TextField(null=True)
