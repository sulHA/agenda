from django.conf.urls import *

from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^$', ContactList.as_view(), name='index'),
    url(r'^nuevo$', ContactCreation.as_view(), name='new'),
    url(r'^editar$', ContactEdit.as_view(), name='edit'),
    url(r'^editar(?P<id>[0-9]+)$', ContactEdit.as_view(), name='edit'),
    url(r'^borrar$', ContactDelete.as_view(), name='delete'),
    url(r'^borrar(?P<id>[0-9]+)$', ContactDelete.as_view(), name='delete'),

]